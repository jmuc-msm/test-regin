
## Running the project

```bash
$ docker-compose up
```
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

## Note 

Do not add any kind of environment variable to facilitate deployment