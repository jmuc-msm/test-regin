const compareDates = (firstDate, secondDate) => {
    return !(firstDate.setHours(0, 0, 0, 0) - secondDate.setHours(0, 0, 0, 0) >= 0)
}
function FeedStory(props) {
    const getDate = d => {
        const date = new Date(d)
        const yesterdayDate = new Date()
        yesterdayDate.setDate(yesterdayDate.getDate() - 1)
        if (!compareDates(yesterdayDate, new Date(d))) {
            return 'Yesterday'
        }
        yesterdayDate.setDate(yesterdayDate.getDate() - 1)
        if (!compareDates(yesterdayDate, new Date(d))) {
            const month = new Intl.DateTimeFormat('en', { month: 'short' }).format(date)
            const day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date)
            return `${month} ${day}`
        }
        return `${date.toTimeString().slice(0, 5)} ${date.toLocaleTimeString().slice(8)}`
    }
    return (
        <ul>
            {props.stories.map(story => (
                <li key={story._id}>
                    <a href={story.story_url || story.url} target="_blank" rel="noreferrer">
                        <p>
                            {story.story_title || story.title} <span className="author">- {story.author} -</span>
                        </p>
                        <span>{getDate(story.created_at)}</span>
                    </a>
                    <button onClick={() => props.removeStory(story._id)}>
                        {!props.loadings.includes(story._id) ? (
                            <svg aria-hidden="true" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <path
                                    fill="currentColor"
                                    d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path>
                            </svg>
                        ) : (
                            <svg className="loading-story" aria-hidden="true" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                <path
                                    fill="currentColor"
                                    d="M456.433 371.72l-27.79-16.045c-7.192-4.152-10.052-13.136-6.487-20.636 25.82-54.328 23.566-118.602-6.768-171.03-30.265-52.529-84.802-86.621-144.76-91.424C262.35 71.922 256 64.953 256 56.649V24.56c0-9.31 7.916-16.609 17.204-15.96 81.795 5.717 156.412 51.902 197.611 123.408 41.301 71.385 43.99 159.096 8.042 232.792-4.082 8.369-14.361 11.575-22.424 6.92z"></path>
                            </svg>
                        )}
                    </button>
                </li>
            ))}
        </ul>
    )
}

export default FeedStory
