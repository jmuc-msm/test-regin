import { Component } from 'react'
import './Feed.scss'
import FeedHeader from './FeedHeader'
import FeedStory from './FeedStory'

class Feed extends Component {
    constructor() {
        super()
        this.state = {
            stories: [],
            loadings: []
        }
    }
    async componentDidMount() {
        const stories = await fetch('http://localhost:3000').then(res => res.json())
        this.setState({ stories })
    }
    removeStory = async (id) => {
        const loadings = [...this.state.loadings, id]
        this.setState({ loadings })
        await fetch(`http://localhost:3000/${id}`, { method: 'DELETE' })
        loadings.splice(loadings.indexOf(id), 1)
        this.setState({ loadings })
        const stories = [...this.state.stories]
        const index = stories.map(({ _id }) => _id).indexOf(id)
        if (index !== -1) {
            stories.splice(index, 1)
            this.setState({ stories })
        }
    }
    render() {
        return (
            <section className="feed">
                <FeedHeader />
                <FeedStory stories={this.state.stories} removeStory={this.removeStory} loadings={this.state.loadings} />
            </section>
        )
    }
}

export default Feed
