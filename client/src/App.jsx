import { Component } from "react";
import Feed from "./feed/Feed";

class App extends Component {
  render() {
    return (
      <Feed />
    );
  }
}

export default App;
