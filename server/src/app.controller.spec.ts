import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
    appService = app.get<AppService>(AppService);
  });

  describe('findAllStories', () => {
    it('should return an array of stories', async () => {
      const result = [];
      jest.spyOn(appService, 'findAllStories').mockImplementation(() => new Promise((resolve, reject) => resolve(result)));

      expect(await appController.findAllStories()).toBe(result);
    });
  });
  
});


