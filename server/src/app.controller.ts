import { Controller, Get, Delete, Param } from '@nestjs/common';
import { AppService } from './app.service';
import Story from './model/story';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async findAllStories() {
    return await this.appService.findAllStories();
  }

  @Delete(':_id')
  async removeStory(@Param('_id') _id: string) {
    return await this.appService.removeStory(_id)
  }
}
