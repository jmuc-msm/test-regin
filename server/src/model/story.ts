import { Schema, model } from 'mongoose';

const story = new Schema({
  story_title: String,
  title: String,
  story_url: String,
  url: String,
  author: String,
  removed: {
    type: Boolean,
    default: null,
  },
  created_at: {
    type: Date,
    require: true,
  },
  objectID: {
    type: String,
    require: true,
    unique: true,
  },
});


const Story = model('Story', story);

export default Story;
