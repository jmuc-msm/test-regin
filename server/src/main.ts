import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import fillData from './middleware/fill-data';
import connectDB from './middleware/mongodb';

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule);
    await connectDB();
    app.enableCors();
    await fillData();
    await app.listen(3000, (): void => {
      console.log('Server Running in port 3000');
    });
  } catch (error) {
    console.error(error);
    throw new Error('Error al conectar con el Api');
  }
}
bootstrap();
