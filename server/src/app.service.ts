import { Injectable } from '@nestjs/common';
import Story from './model/story';

@Injectable()
export class AppService {
  async findAllStories() {
    const stories = await Story.find({ removed: null }).sort({
      created_at: -1,
    });
    return stories;
  }
  async removeStory(_id) {
    await Story.updateOne({ _id }, { removed: true });
    const story = Story.findById(_id);
    return story;
  }
}
