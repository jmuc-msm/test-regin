import fetch from 'node-fetch';
import Story from '../model/story';

export default async function fillData() {
  const stories = await fetch(
    'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
  )
    .then((response) => response.json())
    .then(async (data) => {
      return data.hits
        .filter(({ story_title, title }) => story_title || title)
        .map((story) => ({
          story_title: story.story_title,
          title: story.title,
          story_url: story.story_url,
          url: story.url,
          objectID: story.objectID,
          created_at: story.created_at,
          author: story.author,
        }));
    });
  await Story.bulkWrite(
    stories.map((story) => ({
      updateOne: {
        filter: { objectID: story.objectID },
        update: { $set: story },
        upsert: true,
      },
    })),
  );

  setInterval(() => fillData(), 3600000)
}
