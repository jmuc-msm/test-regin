import { connect } from 'mongoose';

const connectDB = async () => {
  // Use new db connection
  await connect(`mongodb://root:secret@mongodb:27017`, {
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useNewUrlParser: true,
  });
};

export default connectDB;
